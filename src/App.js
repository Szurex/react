import React, {Component} from 'react';

import './App.css';

function  searchingFor(term){
    return function(x){
        if(x.name.toLowerCase().indexOf(term.toLowerCase()) != -1){

        return x;}
    }
}

  class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            items: [],
            isLoaded:false,
            term: '',

        }
        this.searchHandler = this.searchHandler.bind(this);
    }

    componentDidMount(){
        
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(json =>{
                this.setState({
                    isLoaded:true,
                    items: json,
                })
            });
    }
    searchHandler(event){
        debugger;
        this.setState({term: event.target.value});

    }
    render(){

        var{ isLoaded, items, term} = this.state;

        if (!isLoaded){
            return <div>Loading...</div>;
        }

        else{

            return(

                <div className="App">
                <h1>User List</h1>
                <input placeholder="Search People" value={term} onChange={this.searchHandler}></input>
                    <ul>
                      {items.filter(searchingFor(this.state.term)).map(item => (
                        <li key={item.id}>
                        <a class="username">{item.id}.</a> {item.name} <a class="username">@{item.username}</a></li>
                      ))}
                    </ul>
                </div>
            );
        }

    }
}



export default App;
